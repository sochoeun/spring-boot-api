package com.example.server.repo;

import com.example.server.models.Product;
import com.example.server.models.ProductUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductUnitRepository extends JpaRepository<ProductUnit,Integer> {

    List<ProductUnit> getAllByProduct_IdOrderBySaleDefaultDesc(Integer productId);

    ProductUnit findByProduct_IdAndUnitTypes_Id(Integer productId,Integer unitTypeId);

}
