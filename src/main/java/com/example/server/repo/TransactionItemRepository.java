package com.example.server.repo;

import com.example.server.models.TransactionItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionItemRepository extends JpaRepository<TransactionItem,Integer> {
    List<TransactionItem> findAllByTransactionId(Integer id);
}
