package com.example.server.constants;

public interface Constants {
    String STATUS_ACTIVE = "ACTIVE";
    String STATUS_DELETE = "DELETE";
    String STATUS_DISABLE = "DISABLE";

    String NOT_FOUND = "ERR-404";

    String REQUIRED = "ERR-500";

    String EXISTING = "ERR-401";
    String UNEXPECTED = "ERR-501";

    public static final String PHOTO_DIRECTORY = System.getProperty("user.home") + "/Downloads/uploads/";
}
