package com.example.server.exceptons;

public class NotFoundException extends RuntimeException{
    public NotFoundException(String message){
        super(message);
    }
}
