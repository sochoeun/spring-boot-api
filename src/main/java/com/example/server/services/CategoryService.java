package com.example.server.services;

import com.example.server.models.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategories(String status);
    Category getCategoryById(Integer id);
    Category createCategory(Category request);
    Category updateCategory(Category request);

    void deleteCategory(Integer categoryId);


}
