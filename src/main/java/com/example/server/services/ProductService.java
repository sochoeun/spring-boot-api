package com.example.server.services;


import com.example.server.exceptons.WebException;
import com.example.server.models.Product;
import com.example.server.models.ProductUnit;
import com.example.server.models.request.PaginationRequest;
import com.example.server.models.request.ProductUploadRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.Iterator;
import java.util.List;

public interface ProductService {

    Page<Product> getAllProducts(String status, Integer page, Integer size) throws WebException;

    Product getProductById(Integer id) throws WebException;

    void createProduct(Product req) throws WebException;

    void updateProduct(Product re) throws WebException;

    void deleteProduct(Product req) throws WebException;


    /*
    *   Product Unit
    * */
    List<ProductUnit> getAllProductUnit();

    List<ProductUnit> getAllProductUnitByProductId(Integer productId) throws WebException;

    void createProductUnit(ProductUnit req) throws WebException;
    void updateProductUnit(ProductUnit req) throws  WebException;
    void deleteProductUnit(ProductUnit req) throws WebException;

    // upload product image
    String uploadProductImage(Integer productId, MultipartFile file);

    // find all products by category name
    List<Product> getAllProductByCategoryName(String categoryName);
}
