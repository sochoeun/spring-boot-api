package com.example.server.services;

import com.example.server.exceptons.WebException;
import com.example.server.models.Transaction;
import com.example.server.models.TransactionItem;
import com.example.server.models.response.OrderReportResponse;

import java.time.LocalDateTime;
import java.util.List;

public interface TransactionService {
    void createTransaction(Transaction req) throws WebException;
   // List<Transaction> getTransactions();
    List<TransactionItem> getTransactionItems();
    List<TransactionItem> getTransactionItemsById(Integer id);

    // transaction order
    void orderTransaction(Transaction request);
    // transaction purchase
    void purchaseTransaction(Transaction request);
    List<Transaction> getAllByTransactionTypeOptional(String transactionType);




}
