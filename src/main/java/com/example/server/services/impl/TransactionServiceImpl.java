package com.example.server.services.impl;

import com.example.server.exceptons.WebException;
import com.example.server.models.Product;
import com.example.server.models.Transaction;
import com.example.server.models.TransactionItem;
import com.example.server.models.response.OrderReportResponse;
import com.example.server.repo.ProductRepository;
import com.example.server.repo.TransactionItemRepository;
import com.example.server.repo.TransactionRepository;
import com.example.server.services.ProductService;
import com.example.server.services.TransactionService;
import com.example.server.services.UnitTypesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository;
    private final TransactionItemRepository transactionItemRepository;
    private final ProductService productService;
    private final ProductRepository productRepository;
    private final UnitTypesService unitTypesService;
    @Override
    public void createTransaction(Transaction req) throws WebException {

        req.setTransactionDate(LocalDateTime.now());
        req.setStatus("ACTIVE");
        Transaction transaction = transactionRepository.save(req);
        if (transaction.getId() != 0){
            req.getItems().forEach(item ->{
                item.setTransactionId(transaction.getId());
                transactionItemRepository.save(item);
            });
        }
    }

    /*@Override
    public List<Transaction> getTransactions() {
        List<Transaction> all = transactionRepository.findAll();
        all.forEach( a ->{
            List<TransactionItem> list = getTransactionItemsById(a.getId());
            a.setItems(list);
        });
        return all;
    }*/

    @Override
    public List<TransactionItem> getTransactionItems() {
        return transactionItemRepository.findAll();
    }

    @Override
    public List<TransactionItem> getTransactionItemsById(Integer id) {
        return transactionItemRepository.findAllByTransactionId(id);
    }

    // order transaction
    @Override
    public void orderTransaction(Transaction request) {
        // update for order products
        List<Product> productList = request.getItems().stream()
                .map(product -> {
                    var id = product.getProductId();
                    var qty = product.getQty();
                    try {
                        Product productById = productService.getProductById(id);
                        productById.setQtyOnHand(qty);
                        return productById;
                    } catch (WebException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
        productRepository.saveAll(productList);

        request.setTransactionType("ORDER");
        request.setTransactionDate(LocalDateTime.now());
        request.setStatus("ACTIVE");
        Transaction transaction = transactionRepository.save(request);
        if (transaction.getId() != 0){
            request.getItems().forEach(item ->{
                var qty = new BigDecimal(item.getQty());
                item.setTransactionId(transaction.getId());
                item.setTotal(item.getCost().multiply(qty));
                transactionItemRepository.save(item);
            });
        }

        List<TransactionItem> allByTransactionId = transactionItemRepository.findAllByTransactionId(transaction.getId());
        List<BigDecimal> totalsList = allByTransactionId.stream()
                .map(TransactionItem::getTotal)
                .toList();

        BigDecimal totalAmount = BigDecimal.ZERO;
        for (BigDecimal item : totalsList) {
            totalAmount =totalAmount.add(item);
        }

        Transaction updateAmount = transactionRepository.findById(transaction.getId()).orElseThrow();
        updateAmount.setAmount(totalAmount);
        transactionRepository.save(updateAmount);

    }


    @Override
    public void purchaseTransaction(Transaction request) {
        List<Product> productList = request.getItems().stream()
                .map(product -> {
                    var id = product.getProductId();
                    var qty = product.getQty();
                    try {
                        Product productById = productService.getProductById(id);
                        double qtyOnHand = productById.getQtyOnHand();
                        if(qtyOnHand < qty){
                            return null;
                        }
                        productById.setQtyOnHand(qtyOnHand - qty);
                        return productById;
                    } catch (WebException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
        productRepository.saveAll(productList);

        request.setTransactionDate(LocalDateTime.now());
        request.setTransactionType("PURCHASE");

        request.setStatus("ACT");
        Transaction transaction = transactionRepository.save(request);
        if (transaction.getId() != 0){
            request.getItems().forEach(item ->{
                var qty = new BigDecimal(item.getQty());
                item.setTransactionId(transaction.getId());
                item.setTotal(item.getPrice().multiply(qty));
                transactionItemRepository.save(item);
            });
        }

        List<TransactionItem> allByTransactionId = transactionItemRepository.findAllByTransactionId(transaction.getId());
        List<BigDecimal> totalsList = allByTransactionId.stream()
                .map(amount -> amount.getTotal())
                .collect(Collectors.toList());

        BigDecimal totalAmount = BigDecimal.ZERO;
        for (BigDecimal item : totalsList) {
            totalAmount =totalAmount.add(item);
        }
        Transaction updateAmount = transactionRepository.findById(transaction.getId()).orElseThrow();
        updateAmount.setAmount(totalAmount);
        transactionRepository.save(updateAmount);
    }

    @Override
    public List<Transaction> getAllByTransactionTypeOptional(String transactionType) {

        if (transactionType != null){
            List<Transaction> all = transactionRepository.findAllByTransactionType(transactionType);
            all.forEach( a ->{
                List<TransactionItem> list = getTransactionItemsById(a.getId());
                a.setItems(list);
            });
            return all;
        }
        List<Transaction> all = transactionRepository.findAll();
        all.forEach( a ->{
            List<TransactionItem> list = getTransactionItemsById(a.getId());
            a.setItems(list);
        });

        return all;
    }
}
