package com.example.server.services.impl;

import com.example.server.constants.Constants;
import com.example.server.exceptons.WebException;
import com.example.server.models.UnitTypes;
import com.example.server.repo.UnitTypesRepository;
import com.example.server.services.UnitTypesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UnitTypesServiceImpl implements UnitTypesService {
    private final UnitTypesRepository unitTypesRepository;
    @Override
    public List<UnitTypes> findAll() throws WebException {
        return unitTypesRepository.findAll();
    }

    @Override
    public UnitTypes getById(Integer id) throws WebException {
        var checkId = unitTypesRepository.findById(id);
        if(checkId.isEmpty()){
            throw new WebException("UnitTypes ID : " + id + " Not Found","UnitTypes ID : " + id + " Not Found",Constants.NOT_FOUND);
        }
        return unitTypesRepository.findById(id).orElse(null);
    }

    @Override
    public UnitTypes create(UnitTypes req) throws WebException{
        var nameUnique = unitTypesRepository.findByName(req.getName());
        if(nameUnique != null){
            throw new WebException("UnitTypes already Exist","UnitTypes already Exist",Constants.EXISTING);
        }
        req.setStatus("ACTIVE");
        return unitTypesRepository.save(req);
    }

    @Override
    public void delete(Integer id)throws WebException {
        var checkId = unitTypesRepository.findById(id);
        if(checkId.isEmpty()){
            throw new WebException("UnitTypes Not Found","UnitTypes Not Found",Constants.NOT_FOUND);
        }
        unitTypesRepository.deleteById(id);
    }

    @Override
    public UnitTypes update(UnitTypes req) throws WebException{
        UnitTypes unitTypes = getById(req.getId());
        unitTypes.setName(req.getName());
        unitTypes.setStatus(req.getStatus());
        return unitTypesRepository.save(req);
    }
}
