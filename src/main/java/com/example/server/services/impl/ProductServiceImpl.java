package com.example.server.services.impl;

import com.example.server.constants.Constants;
import com.example.server.exceptons.NotFoundException;
import com.example.server.exceptons.WebException;
import com.example.server.file.FileUpload;
import com.example.server.models.Product;
import com.example.server.models.ProductUnit;
import com.example.server.repo.CategoryRepository;
import com.example.server.repo.ProductRepository;
import com.example.server.repo.ProductUnitRepository;
import com.example.server.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Calendar;
import java.util.List;

import static com.example.server.constants.Constants.PHOTO_DIRECTORY;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ProductUnitRepository productUnitRepository;
    private final FileUpload fileUpload;
   // public static final String path = System.getProperty("user.home") + "/Downloads/uploads/";
    @Value("${application.upload.path}"+"products/")
    String path;

    @Override
    public Page<Product> getAllProducts(String status, Integer page, Integer size) throws WebException {
        if(status != null){
            return productRepository.findAllByStatusContainingIgnoreCaseOrderById(status, PageRequest.of(page-1,size));
        }
        return productRepository.findAll(PageRequest.of(page-1,size));
    }

    @Override
    public Product getProductById(Integer id) throws WebException  {
        var product = productRepository.findById(id);
        log.info("Find Product {} ",product);
        if(product.isEmpty()){
            throw  new WebException("Product Not Found!","Product Not Found!", Constants.NOT_FOUND);
        }
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public void createProduct(Product req) throws WebException {
        // check category exist or not
        var category = categoryRepository.findById(req.getCategory().getId());

        if(category.isEmpty()){
            throw new WebException("Category Not Found","Category Not Found",Constants.NOT_FOUND);
        }

        // check product required
        if(null == req.getCode() || "".equals(req.getBarcode()) || "".equals(req.getName())){
            throw new WebException("Name,Code,BarCode is required","Name,Code,BarCode is required",Constants.REQUIRED);
        }

        // check duplicate item
        var code = productRepository.findByCode(req.getCode());
        if(code.isPresent()){
            throw new WebException("Code is existing","Code is existing",Constants.EXISTING);
        }

        var barCode = productRepository.findByBarcode(req.getBarcode());
        if(barCode.isPresent()){
            throw new WebException("BarCode is existing","BarCode is existing",Constants.EXISTING);
        }

        req.setStatus(Constants.STATUS_ACTIVE);
        req.setId(0);
        productRepository.save(req);
    }

    @Override
    public void updateProduct(Product req) throws WebException {
        // check product
        var product = productRepository.findById(req.getId());
        if(product.isEmpty()){
            throw new WebException("Product Not Found","Product Not Found",Constants.NOT_FOUND);
        }
        // check category exist or not
        var category = categoryRepository.findById(req.getCategory().getId());
        if(category.isEmpty()){
            throw new WebException("Category Not Found","Category Not Found",Constants.NOT_FOUND);
        }

        var code = productRepository.findByCode(req.getCode());
        if(code.isPresent()){
            if(!code.get().getCode().equals(product.get().getCode())){
                throw new WebException("Code is existing","Code is existing",Constants.EXISTING);
            }
        }

        var barCode = productRepository.findByBarcode(req.getBarcode());
        if(barCode.isPresent()){
            if(!barCode.get().getBarcode().equals(product.get().getBarcode())){
                throw new WebException("BarCode is existing","BarCode is existing",Constants.EXISTING);
            }
        }
        productRepository.save(req);
    }

    @Override
    public void deleteProduct(Product req) throws WebException {
        var product = productRepository.findById(req.getId());
        if(product.isEmpty()){
            throw new WebException("Product Not Found","Product Not Found",Constants.NOT_FOUND);
        }
        req.setStatus(Constants.STATUS_DELETE);

        productRepository.save(req);
    }
    // upload product image
    @Override
    public String uploadProductImage(Integer proId, MultipartFile file) {
        Product product = productRepository.findById(proId).orElseThrow(
                () -> new NotFoundException("Product ID:%s not found".formatted(proId))
        );
        String fileName =String.valueOf(Calendar.getInstance().getTimeInMillis());
       // String imageUrl = photoFunction.apply(fileName,file);
        String imageUrl = fileUpload.generateUrl(path,fileName,file);
        log.info("Intercept file location: .%s, with file name: %s".formatted(PHOTO_DIRECTORY,fileName));
        product.setImage(imageUrl);
        productRepository.save(product);
        return imageUrl;
    }

    /*
    *   Product Unit
    *
    * */

    @Override
    public List<ProductUnit> getAllProductUnit() {
        return productUnitRepository.findAll();
    }

    @Override
    public List<ProductUnit> getAllProductUnitByProductId(Integer productId) throws WebException{
        return productUnitRepository.getAllByProduct_IdOrderBySaleDefaultDesc(productId);
    }

    @Override
    public void createProductUnit(ProductUnit req) throws WebException {
        // check product exist or not
        // check unitTypes exist or not
        var unitExist = productUnitRepository.findByProduct_IdAndUnitTypes_Id(req.getProduct().getId(),req.getUnitTypes().getId());
        if(unitExist != null)
        {
            throw new WebException("Product Unit is Existing","Product Unit is Existing",Constants.EXISTING);
        }
        productUnitRepository.save(req);
    }

    @Override
    public void updateProductUnit(ProductUnit req) throws WebException {
        // check productUnit
        var isExist = productUnitRepository.findById( req.getId());
        if(isExist.isEmpty()){
            throw new WebException("ProductUnit Not Found","ProductUnit Not Found",Constants.NOT_FOUND);
        }

         // check product
        var product = productRepository.findById(req.getProduct().getId());
        if(product.isEmpty()){
            throw new WebException("Product Not Found","Product Not Found",Constants.NOT_FOUND);
        }
         // check unitTypes
        var unitTypes = productUnitRepository.findById(req.getUnitTypes().getId());
        if(unitTypes.isEmpty()){
            throw new WebException("UnitTypes Existing","UnitTypes Existing",Constants.NOT_FOUND);
        }else{
            if(unitTypes.get().getUnitTypes().getId() == req.getUnitTypes().getId()){
                throw new WebException("UnitTypes Existing","UnitTypes Existing",Constants.EXISTING);
            }
        }
        productUnitRepository.save(req);
    }

    @Override
    public void deleteProductUnit(ProductUnit req) throws WebException {
        var isExist = productUnitRepository.findById( req.getId());
        if(isExist.isEmpty()){
            throw new WebException("ProductUnit Not Found","ProductUnit Not Found",Constants.NOT_FOUND);
        }
        productUnitRepository.delete(req);
    }


    // find all products by category name
    @Override
    public List<Product> getAllProductByCategoryName(String categoryName) {
        // find category by category name
        return productRepository.findAllByCategoryName(categoryName);
    }

}
