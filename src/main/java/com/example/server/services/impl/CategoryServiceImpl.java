package com.example.server.services.impl;

import com.example.server.exceptons.NotFoundException;
import com.example.server.models.Category;
import com.example.server.repo.CategoryRepository;
import com.example.server.services.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryServiceImpl implements CategoryService {
    // constructor injection
    private final CategoryRepository categoryRepository;
    @Override
    public List<Category> getAllCategories(String status) {
        if (status!= null){
            return categoryRepository.findAllByStatusOrderById(status);
        }
        return categoryRepository.findAll();
    }

    @Override
    public Category getCategoryById(Integer categoryId){
        return categoryRepository.findById(categoryId).orElseThrow(
                ()-> new NotFoundException("Category with ID:%s not found".formatted(categoryId))
        );
    }

    @Override
    public Category createCategory(Category request) {
       request.setStatus("ACTIVE");
        return categoryRepository.save(request);
    }

    @Override
    public Category updateCategory(Category request) {
        Category updated = getCategoryById(request.getId());
        updated.setName(request.getName());
        updated.setStatus(request.getStatus());
        return categoryRepository.save(updated);
    }

    @Override
    public void deleteCategory(Integer categoryId){
        Category deleted = getCategoryById(categoryId);
        deleted.setStatus("DELETED");
        categoryRepository.save(deleted);
    }

}
