package com.example.server.services;

import com.example.server.exceptons.WebException;
import com.example.server.models.UnitTypes;

import java.util.List;

public interface UnitTypesService {

    List<UnitTypes> findAll() throws WebException;

    UnitTypes getById(Integer id) throws WebException;

    UnitTypes create(UnitTypes request) throws WebException;

    UnitTypes update(UnitTypes request) throws WebException;

    void delete(Integer id) throws WebException;

}
