package com.example.server.models.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NewResponse {
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime timestamp;
    private HttpStatus status;
    private String message;
    private Object object;

    public void getSuccess(Object object){
        this.setTimestamp(LocalDateTime.now());
        this.setStatus(HttpStatus.OK);
        this.setMessage("get success");
        this.setObject(object);
    }

    public void createdSuccess(Object object){
        this.setTimestamp(LocalDateTime.now());
        this.setStatus(HttpStatus.CREATED);
        this.setMessage("get success");
        this.setObject(object);
    }

    public void updatedSuccess(Object object){
        this.setTimestamp(LocalDateTime.now());
        this.setStatus(HttpStatus.ACCEPTED);
        this.setMessage("get success");
        this.setObject(object);
    }

    public void deletedSuccess(){
        this.setTimestamp(LocalDateTime.now());
        this.setStatus(HttpStatus.GONE);
        this.setMessage("deleted success");
    }

}
