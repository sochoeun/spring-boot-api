package com.example.server.models.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductUploadRequest {
    private String imageUrl;
}
