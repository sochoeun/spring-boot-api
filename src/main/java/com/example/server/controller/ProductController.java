package com.example.server.controller;

import com.example.server.exceptons.WebException;
import com.example.server.models.Product;
import com.example.server.models.ProductUnit;
import com.example.server.models.response.BaseResponse;
import com.example.server.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.example.server.constants.Constants.PHOTO_DIRECTORY;
import static org.springframework.util.MimeTypeUtils.*;

@Controller
@RequiredArgsConstructor
@Slf4j
@CrossOrigin("http://localhost:5173/")
public class ProductController {
    private final ProductService productService;
    private BaseResponse baseResponse;
    @Value("${application.upload.path}"+"products/")
    String path;

    @GetMapping("/api/products")
    public ResponseEntity<?> getAllProducts(
            @RequestParam(required = false) String status,
            @RequestParam(required = false,defaultValue = "1") Integer page,
            @RequestParam(required = false,defaultValue = "10") Integer size){

        log.info("Intercept get all products");
        baseResponse = new BaseResponse();
        try {
            var list = productService.getAllProducts(status, page, size);
            baseResponse.getSuccess(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error get all products ",e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get All products  Unsuccessful");
            baseResponse.setMessageKh("បរាជ័យក្នុងការទាញយក products ទាំងអស់");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    @GetMapping("/api/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable("id") Integer id){
        log.info("Intercept get products by id {}",id);
        baseResponse = new BaseResponse();
        try {
            var list = productService.getProductById(id);
            baseResponse.getSuccess(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (WebException e){
            log.info("While get error products by id ",e);
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/api/products/create")
    public ResponseEntity<Object> create(@RequestBody Product req){
        log.info("Intercept create products {}",req);
        baseResponse = new BaseResponse();
        try {
            productService.createProduct(req);
            baseResponse.getSuccess(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
        }
        catch (WebException e){
            log.info("While get error products by id ",e);
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/api/products/update")
    public ResponseEntity<Object> update(@RequestBody Product req){
        log.info("Intercept update products {}",req);
        baseResponse = new BaseResponse();
        try {
            productService.updateProduct(req);
            baseResponse.updateSuccess(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (WebException e){
            log.info("While get error update products fail",e);
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/api/products/delete")
    public ResponseEntity<Object> delete(@RequestBody Product req){
        log.info("Intercept delete products {}",req);
        baseResponse = new BaseResponse();
        try {
            productService.deleteProduct(req);
            baseResponse.deleteSuccess(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        catch (WebException e){
            log.info("While get error update products fail",e);
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    // upload product image
    @PostMapping(value = "/api/products/upload/image",consumes = "multipart/form-data")
    public ResponseEntity<?> uploadProductImage(
                @RequestParam Integer proId,
            @RequestParam MultipartFile file
    ){
        String imageUrl = productService.uploadProductImage(proId, file);
       // log.info("Intercept upload product image in Path {} ", imageUrl);
        return ResponseEntity.ok(imageUrl);
    }
    @GetMapping(value = "/api/products/image/{filename}",produces = {IMAGE_PNG_VALUE,IMAGE_JPEG_VALUE,ALL_VALUE})
    public ResponseEntity<byte[]> getProfile(@PathVariable("filename") String filename) throws Exception{
        byte[] bytes = Files.readAllBytes(Paths.get(path + filename));
        return ResponseEntity.ok(bytes);
    }

    /*
    *   Product Unit
    * */
    @GetMapping("/api/products/units/{productId}")
    public ResponseEntity<Object> getAllProductUnitByProductId(@PathVariable("productId") Integer productId){
        log.info("Intercept get all products unit");
        baseResponse = new BaseResponse();
        try {
            var list = productService.getAllProductUnitByProductId(productId);
            baseResponse.getSuccess(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error get all products ",e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get All products  Unsuccessful");
            baseResponse.setMessageKh("បរាជ័យក្នុងការទាញយក products ទាំងអស់");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }

    }

    @GetMapping("/api/products/unit")
    public ResponseEntity<Object> getAllProductUnits(){
        log.info("Intercept get all products unit");
        baseResponse = new BaseResponse();
        try {
            var list = productService.getAllProductUnit();
            baseResponse.getSuccess(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error get all products ",e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get All products  Unsuccessful");
            baseResponse.setMessageKh("បរាជ័យក្នុងការទាញយក products ទាំងអស់");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }

    }

    @PostMapping("/api/products/units/create")
    public ResponseEntity<Object> createProductUnit(@RequestBody ProductUnit req){
        log.info("Intercept create products {}",req);
        baseResponse = new BaseResponse();
        try {
            productService.createProductUnit(req);
            baseResponse.createSuccess(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (WebException e){
            log.info("While create error products unit ",e);
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }
    @PostMapping("/api/products/units/update")
    public ResponseEntity<Object> updateProductUnit(@RequestBody ProductUnit req){
        log.info("Intercept update products {}",req);
        baseResponse = new BaseResponse();
        try {
            productService.updateProductUnit(req);
            baseResponse.updateSuccess(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (WebException e){
            log.info("While update error products unit ",e);
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @PostMapping("/api/products/units/delete")
    public ResponseEntity<Object> deleteProductUnit(@RequestBody ProductUnit req){
        log.info("Intercept update products {}",req);
        baseResponse = new BaseResponse();
        try {
            productService.deleteProductUnit(req);
            baseResponse.deleteSuccess(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (WebException e){
            log.info("While delete error products unit ",e);
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }


    // get all products by category name
    @GetMapping("/api/products/category/{categoryName}")
    public ResponseEntity<?> getAllProductByCategoryName(@PathVariable String categoryName){
        List<Product> allProductByCategoryName = productService.getAllProductByCategoryName(categoryName);

        return ResponseEntity.ok(allProductByCategoryName);
    }
}
