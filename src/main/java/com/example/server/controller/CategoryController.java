package com.example.server.controller;

import com.example.server.models.Category;
import com.example.server.models.response.NewResponse;
import com.example.server.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequiredArgsConstructor
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService categoryService;
    private NewResponse newResponse;
    @GetMapping()
    public ResponseEntity<Object> getAllCategory(@RequestParam (required = false) String status){
        List<Category> allCategories = categoryService.getAllCategories(status);
        newResponse = new NewResponse();
        newResponse.getSuccess(allCategories);
        return ResponseEntity.ok(newResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") Integer id){
        Category categoryById = categoryService.getCategoryById(id);
        newResponse = new NewResponse();
        newResponse.getSuccess(categoryById);
        return ResponseEntity.ok(newResponse);
    }

    @PostMapping()
    public ResponseEntity<Object> createCategory(@RequestBody Category request){
        Category category = categoryService.createCategory(request);
        newResponse = new NewResponse();
        newResponse.getSuccess(category);
        return ResponseEntity.ok(newResponse);
    }

    @PostMapping("/update")
    public ResponseEntity<Object> updateCategory(@RequestBody Category request){
        Category category = categoryService.updateCategory(request);
        newResponse = new NewResponse();
        newResponse.updatedSuccess(category);
        return ResponseEntity.ok(newResponse);
    }

    @PostMapping("/delete/{categoryId}")
    public ResponseEntity<Object> deleteCategory(@PathVariable Integer categoryId){
       categoryService.deleteCategory(categoryId);
       newResponse = new NewResponse();
       newResponse.deletedSuccess();
       return ResponseEntity.ok(newResponse);
    }

}
