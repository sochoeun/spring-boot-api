package com.example.server.controller;

import com.example.server.exceptons.WebException;
import com.example.server.models.Transaction;
import com.example.server.models.response.BaseResponse;
import com.example.server.services.TransactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/transactions")
@RequiredArgsConstructor
@Slf4j
public class TransactionController {
    private final TransactionService transactionService;
    private BaseResponse baseResponse;
    @PostMapping("/create")
    public ResponseEntity<Object> create(@RequestBody Transaction req){
        log.info("Intercept create transaction {}",req);
        baseResponse = new BaseResponse();
        try {
            transactionService.createTransaction(req);
            baseResponse.getSuccess(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (WebException e){
            log.info("While get error transaction by id ",e);
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }


    /*@GetMapping()
    public ResponseEntity<Object> getTransactions(){
        log.info("Intercept get all transaction");
        baseResponse = new BaseResponse();
        try {
            var list = transactionService.getTransactions();
            baseResponse.getSuccess(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error get all transaction ",e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get All transaction  Unsuccessful");
            baseResponse.setMessageKh("បរាជ័យក្នុងការទាញយក transaction ទាំងអស់");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }

    }*/
    @GetMapping("/items")
    public ResponseEntity<Object> getTransactionItems(){
        log.info("Intercept get all transaction item");
        baseResponse = new BaseResponse();
        try {
            var list = transactionService.getTransactionItems();
            baseResponse.getSuccess(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error get all transaction ",e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get All transaction item Unsuccessful");
            baseResponse.setMessageKh("បរាជ័យក្នុងការទាញយក transaction item ទាំងអស់");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }

    }
    @GetMapping("/{id}/items")
    public ResponseEntity<Object> getTransactionItemsById(@PathVariable("id") Integer id){
        log.info("Intercept get all transaction item");
        baseResponse = new BaseResponse();
        try {
            var list = transactionService.getTransactionItemsById(id);
            baseResponse.getSuccess(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error get all transaction ",e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get All transaction item Unsuccessful");
            baseResponse.setMessageKh("បរាជ័យក្នុងការទាញយក transaction item ទាំងអស់");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }

    }

    @PostMapping("/order")
    public ResponseEntity<Object> order(@RequestBody Transaction req){
        log.info("Intercept create transaction {}",req);
        baseResponse = new BaseResponse();
        transactionService.orderTransaction(req);
        baseResponse.getSuccess(req);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @PostMapping("/purchase")
    public ResponseEntity<Object> purchase(@RequestBody Transaction req){
        log.info("Intercept create transaction {}",req);
        baseResponse = new BaseResponse();
        transactionService.purchaseTransaction(req);
        baseResponse.getSuccess(req);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @GetMapping("/transaction-type")
    public ResponseEntity<?> getAllByTransactionTypeOptional(@RequestParam(required = false) String tractionType){
        List<Transaction> allByTransactionType = transactionService.getAllByTransactionTypeOptional(tractionType);
        return ResponseEntity.ok(allByTransactionType);
    }

    @GetMapping("/order-report/{dateTime}")
    public ResponseEntity<Object> getOrderReport(@PathVariable("dateTime") LocalDateTime dateTime){
        log.info("Intercept report order");
        try {
           // List<Transaction> list = transactionService.orderReport(dateTime);
            return null;
        }
        catch (Throwable e){
            log.info("While get error get all transaction ",e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }

    }
}
