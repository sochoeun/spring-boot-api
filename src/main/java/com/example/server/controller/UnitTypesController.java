package com.example.server.controller;

import com.example.server.exceptons.WebException;
import com.example.server.models.UnitTypes;
import com.example.server.models.response.BaseResponse;
import com.example.server.services.UnitTypesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@Slf4j
@CrossOrigin("http://localhost:5173/")
public class UnitTypesController {
    private final UnitTypesService service;
    private BaseResponse baseResponse;

    @GetMapping("/api/unitTypes")
    public ResponseEntity<Object> getAllUnitTypes() throws WebException{
        baseResponse = new BaseResponse();
        var list = service.findAll();
        baseResponse.getSuccess(list);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @GetMapping("/api/unitTypes/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") Integer id) throws WebException{
        baseResponse = new BaseResponse();
        try{
            var list = service.getById(id);
            baseResponse.getSuccess(list);
            return new ResponseEntity<>(baseResponse,HttpStatus.OK);
        }catch (WebException e){
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse,HttpStatus.OK);
        }

    }

    @PostMapping("/api/unitTypes/create")
    public ResponseEntity<Object> create(@RequestBody UnitTypes req) throws WebException{
        baseResponse = new BaseResponse();
        try{
            service.create(req);
            baseResponse.createSuccess(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse,HttpStatus.OK);
        }

    }

    @PostMapping("/api/unitTypes/update")
    public ResponseEntity<Object> update(@RequestBody UnitTypes req)throws WebException{
        baseResponse = new BaseResponse();
        try{
            service.update(req);
            baseResponse.updateSuccess(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse,HttpStatus.OK);
        }

    }

    @PostMapping("/api/unitTypes/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Integer id)throws WebException{
        baseResponse = new BaseResponse();
        try {
            var list = service.getById(id);
            baseResponse.deleteSuccess(list);
            service.delete(id);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (WebException e){
            log.info("While delete products error ",e);
            baseResponse.setCode(e.getCode());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

}
